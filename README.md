#	Wiring configuration of in- and output wires of the driver PCB


## Solder Pads:

Not all RGB-strips are wired the same way. You have to check every PCB individually.

Most used are:

## Output Terminal:

| **OUTPUT-PAD**	| **WIRING 1** | **WIRING 2** |
| ----- | -------- | -------- |
| R | 3 | 3 |
| G | **2** | **1** |
| B | **1** | **2** |
| +24V | YEGN | YEGN |

## Input Terminal:

| **PIN** | **WIRE** | **DISCRIPTION** |
| --- | ---- | ----------- |
| 1 | 2 | +24V IN |
| 2 | 3 | RS485-B (-) |
| 3 | 1 | RS485-A (+) |
| 4 | YEGN | 0V (GND) IN |

## Additional Informations:
	Beide Paare (Stromversorgung und RS485 Daten) sind an jene Adern ange-
	schlossen, welche im Kabel gegenüberliegen, um eventuelle Störungen durch
	den fließenden Strom zu verringern, da das verwendete Kabel weder ge-
	schirmt, noch über Twisted-Pair Adern verfügt.

	Der Querschnitt der Adern beträgt 0,75mm².

	Der maximal fließende Strom beträgt 1,85A (alle Farben 100%).